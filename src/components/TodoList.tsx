import React from 'react';
import { TodoListItem } from './TodoListItem';

interface TodoListProps {
  todos: Array<Todo>;
  toggleComplete: ToggleComplete;
  removeTodo: RemoveTodo;
}

export const TodoList: React.FC<TodoListProps> = ({todos, toggleComplete, removeTodo}) => {
  return (
    <ul>
      {todos.map(todo => (
        <TodoListItem
        key={todo.text}
        todo={todo}
        toggleComplete={toggleComplete}
        removeTodo={removeTodo}
        />
      ))

      }
    </ul>
  )
}