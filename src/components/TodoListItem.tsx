import React from 'react';
import DeleteIcon from '@material-ui/icons/Delete';

interface TodoListItemProps {
  todo: Todo;
  toggleComplete: ToggleComplete;
  removeTodo: RemoveTodo;
}

export const TodoListItem: React.FC<TodoListItemProps> = ({ todo, toggleComplete, removeTodo}) => {
  return (
    <li>
      <label className={todo.complete ? "todo-row completed" : "todo-row"}>
        <input 
          type="checkbox"
          onChange={() => toggleComplete(todo)}
          checked={todo.complete}
        />
        <span className="checkmark"></span>
        {todo.text}
      </label>
      {todo.complete && 
        <button className="button-delete" onClick={() => removeTodo(todo)}><DeleteIcon style={{ fontSize: 20 }} /></button>
      }
    </li>
  )
}